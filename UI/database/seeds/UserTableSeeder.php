<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
  public function run()
  {
      DB::table('users')->delete();
      User::create(array(
          'name'     => 'Administrator',
          'email'    => 'admin@admin.com',
          'password' => Hash::make('admin'),
      ));
  }
}
