<!--Navbar-->
<nav class="navbar fixed-top navbar-default navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Cloud Watchdog</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ (Request::is('admin') ? 'active' : '') }} {{ (Request::is('rule/create') ? 'active' : '') }}">
                    <a href="{{ url('/admin') }}"> Admin </a>
                </li>
                <li class="{{ (Request::is('monitor') ? 'active' : '') }}">
                    <a href="{{ url('monitor') }}"> Monitor </a>
                </li>
                <li class="{{ (Request::is('instances') ? 'active' : '') }}">
                    <a href="{{ url('instances') }}"> Manage Instances </a>
                </li>
                <li class="{{ (Request::is('detailedReport') ? 'active' : '') }}">
                    <a href="{{ url('detailedReport') }}"> Detailed Report </a>
                </li>
                <li class="{{ (Request::is('contact') ? 'active' : '') }}">
                    <a href="{{ url('contact') }}"> Guide </a>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Administrator <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('logout') }}">Logout</a></li>
                  </ul>
                </li>
              </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->
