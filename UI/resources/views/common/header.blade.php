<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="Cloud Watchdog">
<meta name="author" content="Altaf Hussian">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Cloud Watchdog</title>

<link rel="stylesheet" href="{{{ URL::asset('css/app.css')}}}" />
<link rel="stylesheet" href="{{{ URL::asset('css/datatables.css')}}}" />
<link rel="stylesheet" href="{{{ URL::asset('css/bootstrap-datepicker.min.css')}}}" />
<link rel="stylesheet" href="{{{ URL::asset('css/bootstrap-datetimepicker.css')}}}" />
<link rel="stylesheet" href="{{{ URL::asset('css/main.css')}}}" />

<script src="{{{ URL::asset('js/app.js') }}}"></script>

 {!! Charts::assets() !!}

</head>
