@include('common.header')

<div class="container-fluid">
  <div class="row">
    <div class="pen-title">
      <h1>Cloud Watchdog</h1>
    </div>
    <div class="form-module">

              <div class="text-center pad-10">
                <img class="img-thumbnail" alt="Cloud Watchdog" src="{{ URL::asset('images/watchdog.jpg') }}" style="width: 50%; height: 50%;padding: 10px"/>
              </div>
                <form class="form-signin" action="{{ url('/monitor') }}" method="post">
                  {!! csrf_field() !!}

                    <input type="username" id="username" class="form-control" placeholder="Username" required autofocus>
                    <input type="password" id="password" class="form-control" placeholder="Password" required>
                    <div id="remember" class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me"> Remember me
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                </form><!-- /form -->
                <p class="text-center">
                  <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                </p>


        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

      </form>
    </div>
  </div>
</div>
