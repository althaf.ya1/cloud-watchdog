@include('common.header')

<head>
  <script type="text/javascript">
  $(function () {
    $('#startDate').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true,
      clearBtn: true,
    });
    $('#endDate').datepicker({
        format: 'dd/mm/yyyy',
        clearBtn: true,
        autoclose: true,
        useCurrent: false //Important! See issue #1075
    });
  });
  </script>
</head>
@include('common.menu')

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <form class="form-inline">
        <fieldset>
        <legend>Cost Savings Search</legend>
      <div class="form-group">
        <label>Start Date</label>
        <div class="input-group date" data-provide="datepicker" id="startDate">
          <input type="text" class="form-control" value="">
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>End Date</label>
        <div class="input-group date" data-provide="datepicker" id="endDate">
          <input type="text" class="form-control" value="">
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class='input-group'>
          <button id="btnSearch" name="btnSearch" class="btn btn-primary">Search</button>
        </div>
      </div>
    </fieldset>
    </form>
  </div>
    </div>
  </br>
    <div class="row">
      <div class="col-md-6">
        {!! $idleChart->render() !!}
      </div>
      <div class="col-md-6">
        {!! $costChart->render() !!}
      </div>
    </div>
  </br>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        {!! $currentChart->render() !!}
      </div>
    </div>
  </div>
</body>
