@include('common.header')

@include('common.menu')

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        {!! $ec2Chart->render() !!}
      </div>
      <div class="col-md-6">
        {!! $idleChart->render() !!}
      </div>
  </div>
</br>
  <div class="row">
    <div class="col-md-6">
        {!! $costChart->render() !!}
    </div>

    <div class="col-md-5 col-md-offset-1">
      <table class="table table-bordered" id="costSavingDetails">
        <thead>
          <tr>
            <th>Date</th>
            <th>Cost (dollar)</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $iterator =  1;
            foreach ($costSavingDetails as $key => $value) {
          ?>
          <tr>
            <th scope="row"><?php echo $value->{'Date'}; ?></th>
            <td><?php echo $value->{'cost saved'}; ?></td>
          </tr>
          <?php
            $iterator++;
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</body>
