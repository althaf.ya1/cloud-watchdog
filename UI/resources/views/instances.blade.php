@include('common.header')

@include('common.menu')

<body>
  <div class="container">
    <div class="col-md-10 col-md-offset-1">
      <table class="table table-hover" id="instancesTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Instance ID with Tag</th>
            <th>Current State</th>
            <th>Action</th>
            <th>Keep EC2 active always</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $iterator =  1;
            foreach ($instanceDetails as $key => $value) {
          ?>
          <tr>
            <th scope="row"><?php echo $iterator; ?></th>
            <td><?php echo $value->instance_id . " - " . $value->instance_name; ?></td>
            <td><a href="#" class="btn nohover <?php echo $value->instance_state == 'off' ? ' btn-danger' : ' btn-success' ?>"><?php echo strtoupper($value->instance_state); ?></a></td>
            <td>
              <label class="action_switch">
                <input type="checkbox" <?php echo $value->instance_state == 'off' ? '' : 'checked' ?>>
                <div class="slider round"></div>
              </label>
            </td>
            <td>
              <select id="ec2_status_1" name="ec2_status" class="form-control">
                <option value="1">LOCK</option>
                <option value="2" selected="">NA</option>
              </select>
            </td>
          </tr>
          <?php
            $iterator++;
          } ?>
        </tbody>
      </table>
    </div>
</div>
</body>
<script>
$(document).ready(function(){
  $('#instancesTable').DataTable();
});
</script>
