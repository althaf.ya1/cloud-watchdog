@include('common.header')
<head>
  <script type="text/javascript">
  $(function () {
    $('#startDateTime').datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true,
      clearBtn: true,
    });
    $('#endDateTime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        clearBtn: true,
        autoclose: true,
    });
  });
  </script>
</head>

@include('common.menu')

<div class="container">
  <div class="col-md-10">
    <form class="form-horizontal">
    <fieldset>
    <!-- Form Name -->
    <legend>Create Rule</legend>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-5 control-label" for="txtRuleName" data-toggle="tooltip" title="Name of the Rule">Rule Name</label>
      <div class="col-md-5">
      <input id="txtRuleName" name="txtRuleName" placeholder="Rule Name" class="form-control input-md" required="" type="text">
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-5 control-label" for="selAWSRegion">AWS Region</label>
      <div class="col-md-5">
        <select id="selAWSRegion" name="selAWSRegion" class="form-control">
          <option value="1">EU (Frankfurt)</option>
          <option value="2">EU (Ireland)</option>
          <option value="3">EU (London)</option>
        </select>
      </div>
    </div>

    <!-- Button Drop Down -->
    <div class="form-group">
      <label class="col-md-5 control-label" for="txtIdleTime">Max Idle Time (No network I/O)</label>
      <div class="col-md-5">
        <div class="input-group">
          <input id="txtIdleTime" name="txtIdleTime" class="form-control" placeholder="Enter Number" type="text">
          <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              Duration
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right">
              <li><a href="#">Hourly</a></li>
              <li><a href="#">Day</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>


    <div class="form-group">
      <label class="col-md-5 control-label" for="selReportPeroid" data-toggle="tooltip" title="Sending report to Your mail">Weekly Shutdown</label>
      <div class="col-md-5">
        <div class="input-group date" data-provide="datepicker" id="startDateTime">
          <input type="text" class="form-control" value="" placeholder="Start DateTime">
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
        <div class="input-group date" data-provide="datepicker" id="endDateTime">
          <input type="text" class="form-control" value="" placeholder="End DateTime">
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-5 control-label" for="selReportPeroid" data-toggle="tooltip" title="Sending report to Your mail">Sending Report Period</label>
      <div class="col-md-5">
        <select id="selReportPeroid" name="selReportPeroid" class="form-control">
          <option value="1">Hourly</option>
          <option value="2">Daily</option>
        </select>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-5 control-label" for="txtStackName" data-toggle="tooltip" title="Stack Name pattern to identify filters">Stack Name (pattern to identify filter)</label>
      <div class="col-md-5">
      <input id="txtStackName" name="txtStackName" placeholder="Tag Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-5 control-label" for="txtInstanceName" data-toggle="tooltip" title="Instance Name pattern to identify filters">Instance Name (pattern to identify filter)</label>
      <div class="col-md-5">
      <input id="txtInstanceName" name="txtInstanceName" placeholder="Tag Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-5 control-label" for="txtTagKey" data-toggle="tooltip" title="Tag Key pattern to identify filters">Instance tag key (pattern to identify filter)</label>
      <div class="col-md-5">
      <input id="txtTagKEy" name="txtTagKey" placeholder="Tag Name" class="form-control input-md" type="text">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-5 control-label" for="txtTagValue" data-toggle="tooltip" title="Tag values pattern to identify filters">Instance tag key (pattern to identify filter)</label>
      <div class="col-md-5">
      <input id="txtTagKey" name="txtTagKey" placeholder="Tag Name" class="form-control input-md" type="text">
      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-5 control-label" for="btnCreateRule"></label>
      <div class="col-md-4">
        <button id="btnCreateRule" name="btnCreateRule" class="btn btn-primary">Create</button>
      </div>
    </div>

    </fieldset>
    </form>

  </div>
</div>
