@include('common.header')

@include('common.menu')

<div class="container">
  <div class="col-md-4 col-md-offset-4">
    <form class="form-horizontal">
    <fieldset>

    <legend>Security Details</legend>
    <div class="control-group">
      <label class="control-label input-label" for="accessKey">Access Key</label>
      <div class="controls pull-right">
          <a href="#" class="btn btn-sm btn-info"> <span class="glyphicon glyphicon-edit"></span></a>
      </div>
    </div>
  </br>
    <div class="control-group">
      <label class="control-label input-label" for="accessKey">Secret Key</label>
      <div class="controls pull-right">
          <a href="#" class="btn btn-sm btn-info"> <span class="glyphicon glyphicon-edit"></span></a>
      </div>
    </div>
    </fieldset>
    </form>

</br></br>

    <form class="form-horizontal">
    <fieldset>

    <legend>  Create Rules
    <div class="control-group pull-right">
      <div class="controls">
        <a href="{{ url('/rule/create') }}" class="btn btn-sm btn-success margin-bottom-10"> <span class="glyphicon glyphicon-plus"></span></a>
      </div>
    </div>
    </legend>

    <div class="control-group">
      <label class="control-label input-label" for="accessKey">Developer</label>
      <div class="controls pull-right">
          <a href="#" class="btn btn-sm btn-info"> <span class="glyphicon glyphicon-edit"></span></a>
      </div>
    </div>
  </br>
    <div class="control-group">
      <label class="control-label input-label" for="accessKey">Tester</label>
      <div class="controls pull-right">
          <a href="#" class="btn btn-sm btn-info"> <span class="glyphicon glyphicon-edit"></span></a>
      </div>
    </div>
    </fieldset>
    </form>
  </div>
</div>
