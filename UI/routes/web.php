<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::any('/admin', function () {
    return view('admin');
});

Route::any('/instances', [
  'as' => 'instances',
  'uses' => 'InstanceController@index'
]);

Route::any('/monitor', [
  'as' => 'monitor',
  'uses' => 'MonitorController@index'
]);

Route::any('/detailedReport', [
  'as' => 'detailedReport',
  'uses' => 'DetailedReportController@index'
]);

Route::get('/logout', function () {
    return view('auth.login');
});

Route::any('/rule/create', function () {
    return view('rule');
});
