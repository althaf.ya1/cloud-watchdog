<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Charts;

class DetailedReportController extends Controller
{
    public function index()
    {
      $idlechart = Charts::create('bar', 'highcharts')
            ->title('Idle Machines')
            ->labels(['First', 'Second', 'Third'])
            ->values([15,10,25])
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("No. of EC2 turned Off");

      $costchart = Charts::create('bar', 'highcharts')
            ->title('Cost Savings')
            ->labels(['First', 'Second', 'Third'])
            ->values([15,10,25])
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("Hours");

      $currentchart = Charts::create('bar', 'highcharts')
            ->title('Current Day Report')
            ->labels(['First', 'Second', 'Third','Fourth','Fifth', 'Sixth', 'Seventh'])
            ->values([5,10,20,15,35,20,10])
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("Type of EC2 deployed");

        return view('detailedReport', ['idleChart' => $idlechart, 'currentChart' => $currentchart, 'costChart' => $costchart ]);
    }
}
