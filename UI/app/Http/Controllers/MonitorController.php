<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Charts;
use GuzzleHttp\Client as GuzzleHttpClient;

class MonitorController extends Controller
{
    public function index()
    {
      $client = new GuzzleHttpClient();
      $ec2Request = $client->request('GET', 'http://52.43.70.22:5000/ec2_matrix');
      $ec2Array = (array) json_decode($ec2Request->getBody()->getContents());

      $idleRequest = $client->request('GET', 'http://52.43.70.22:5000/idle_machines');
      $idleArray = (array) json_decode($idleRequest->getBody()->getContents());

      $costRequest = $client->request('GET', 'http://52.43.70.22:5000/billing_details');
      $costArray = (array) json_decode($costRequest->getBody()->getContents());

      foreach ($costArray as $key => $value) {
        $costArray = str_replace("$","",$value);
      }

      $costArray = ["Apr"=> "1200", "Current" => "200", "Feb" => "3500", "Mar" => "2800"];

      $ec2chart = Charts::create('bar', 'highcharts')
            ->title('EC2 Matrix')
            ->labels(array_keys($ec2Array))
            ->values(array_values($ec2Array))
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("Instance types/Total instances");

      $idlechart = Charts::create('line', 'highcharts')
            ->title('Idle Machines')
            ->labels(array_keys($idleArray))
            ->values(array_values($idleArray))
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("Hours/Idle Count");

      $costchart = Charts::create('line', 'highcharts')
            ->title('Billing Details')
            ->labels(array_keys($costArray))
            ->values(array_values($costArray))
            ->dimensions(0,500)
            ->responsive(true)
            ->elementLabel("Amount/Month");

      $client = new GuzzleHttpClient();
      $idleActionData = $client->request('GET', 'http://52.43.70.22:5000/idle_action_details_table');
      $idleActionDetails = json_decode($idleActionData->getBody()->getContents());

      $costSavingData = $client->request('GET', 'http://52.43.70.22:5000/cost_saving_details_table');
      $costSavingDetails = json_decode($costSavingData->getBody()->getContents());

      return view('monitor', ['idleChart' => $idlechart, 'ec2Chart' => $ec2chart, 'costChart' => $costchart, 'idleActionDetails' => $idleActionDetails, 'costSavingDetails' => $costSavingDetails ]);
    }
}
