<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client as GuzzleHttpClient;

class InstanceController extends Controller
{
    public function index()
    {
      $client = new GuzzleHttpClient();
      $res = $client->request('GET', 'http://52.43.70.22:5000/manage_instances');
      $instanceDetails = json_decode($res->getBody()->getContents());
      
      return view('instances', ['instanceDetails' => $instanceDetails]);
    }
}
