var elixir = require('laravel-elixir');

var paths = {
    'jquery': './node_modules/jquery/',
    'bootstrap': './node_modules/bootstrap-sass/assets/'
}

elixir(function(mix) {
    mix.sass("app.scss", 'public/css/', {includePaths: [paths.bootstrap + 'stylesheets/']})
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts')
        .scripts([
            paths.jquery + "dist/jquery.js",
            paths.bootstrap + "javascripts/bootstrap.js",
            "./resources/assets/js/moment.min.js",
            "./resources/assets/js/bootstrap-datepicker.js",
            "./resources/assets/js/datatables.js",
            "./resources/assets/js/custom.js"
        ], 'public/js/app.js');
});
