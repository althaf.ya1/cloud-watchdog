from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

class testHTTPServer(BaseHTTPRequestHandler):
    def _set_header(self):
        self.send_response(200)
#        self.send_header('Content-type', 'application/json')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_header()
#        f = open("/home/nagendran/out_file.json", "r")
        f = open("/home/nagendran/index.html", "r")
        self.wfile.write(f.read())

    def do_HEAD(self):
        self._set_header()

def run():
  print('starting server...')
  server_address = ('127.0.0.1', 8081)
  httpd = HTTPServer(server_address, testHTTPServer)
  print('running server...')
  httpd.serve_forever()

run()

