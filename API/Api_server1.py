import re, os, sys, time, json
from flask import Flask, jsonify, request

app = Flask(__name__)

from sqlextract import Extract
ExtractObj = Extract()

@app.route("/")
def rootpath():
    return "Hi This is root path!... testing ..."



@app.route('/manage_instances', methods=['GET'])
def manageinstances():
    print request.args
    for i in request.args:
        print i

    if len(request.args) > 0:
        try:
            argsTot = None
            for j in request.args:
                args = j + '=' + '"' + request.args.get(j) + '"'
                print args
                if argsTot:
                    print "inside.."
                    argsTot += ' and '
                    argsTot += args
                else:
                    argsTot = args
                    argsTot = argsTot.replace('%20', ' ')
                   # res = ExtractObj.manageexport('manage_instances', argsTot)
                    return jsonify({'AvailableData':ExtractObj.manageexport('manage_instances',argsTot)})
        except:
            #res = ExtractObj.manageexport('manage_instances', 0)
            return jsonify({'AvailableData':ExtractObj.manageexport('manage_instances',0)})
    else:
        #res = ExtractObj.manageexport('manage_instances', 0)
        return jsonify({'AvailableData':ExtractObj.manageexport('manage_instances',0)})



@app.route('/ec2_matrix',methods=['GET'])
def ec2matrix():
	print request.args
	return jsonify({'Ec2_Matrix':ExtractObj.ec2matrix('manage_instances')})



@app.route('/Idle_machine', methods=['GET'])
def idle_machine():
	return jsonify({'Idle_Machine':ExtractObj.idlemachine('poll_info')})


@app.route('/bill/<instance_id>',methods=['GET'])
def billing(instance_id):
	return jsonify({'BillingDetails':ExtractObj.billing_details(instance_id)})

@app.route('/ec2turn_off_date', methods=['GET'])
def ec2name():
	return jsonify({'BillingDetails':ExtractObj.ec2name('manage_instances')})


if __name__ == "__main__":
    app.run(debug=True)
    app.run(host="0.0.0.0")
    app.run(port=5000)
