import json
import sqlite3
import os, sys, re

class Extract(object):
    def __init__(self):
        pass


    def manageexport(self, table, condition):
        connection = sqlite3.connect("/root/todo-api/flask/sqlite-autoconf-3070603/Cloudwatch.db")
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        if condition == 0:
            cursor.execute("SELECT instance_id, instance_name, stack_name, instance_state, tag_key, tag_value FROM %s where report_ui='yes'" % table)
        else:
            cursor.execute("SELECT instance_id, instance_name, stack_name, instance_state, tag_key, tag_value FROM %s where report_ui='yes' and %s" % (table, condition))

        print("SELECT instance_id, instance_name, stack_name, instance_state, tag_key, tag_value FROM %s where report_ui='yes' and %s" % (table, condition))
        results = cursor.fetchall()
        rows = [ dict(rec) for rec in results ]
        #rows_json = json.dumps(rows)
        connection.close()
        return rows
        #print rows_json



    def ec2matrix(self, table):
        connection = sqlite3.connect("/root/todo-api/flask/sqlite-autoconf-3070603/Cloudwatch.db")
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute(
            "select instance_type, count(instance_type) as total_count from %s where report_ui = 'yes' group by instance_type;" % (table))
        results = cursor.fetchall()
        rows = [dict(rec) for rec in results]
        #print rows
        #rows_json = json.dumps(rows)
        connection.close()
        return rows
        # print rows_json



    def idlemachine(self, table):
	print table
        connection = sqlite3.connect("/root/todo-api/flask/sqlite-autoconf-3070603/Cloudwatch.db")
	connection.row_factory = sqlite3.Row
	cursor = connection.cursor()
	cursor.execute("select instance_type, strftime('%d-%m-%Y  %H', poll_time) POLL_TIME_HR, count(*) IDLE_COUNT from"" "+table+" "" where idle='yes' group by 1,2;")
	results = cursor.fetchall()
	rows = [dict(rec) for rec in results]
	#print rows
	connection.close()
	return rows


    def billing_details(self, billing_details):
	connection = sqlite3.connect("/root/todo-api/flask/sqlite-autoconf-3070603/Cloudwatch.db")
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute("select Month, Amount from billing_details where instance_id=""'"+billing_details+"'")
        results = cursor.fetchall()
        rows = [dict(rec) for rec in results]
        #print rows
        connection.close()
        return rows

    def ec2name(self, table):
	connection = sqlite3.connect("/root/todo-api/flask/sqlite-autoconf-3070603/Cloudwatch.db")
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute("select tag_key as EC2Name, instance_state as CurrentState, strftime('%d-%m-%Y', poll_time)as Turned_off_date from"" " +table+"")
        results = cursor.fetchall()
        rows = [dict(rec) for rec in results]
        #print rows
        connection.close()
        return rows

