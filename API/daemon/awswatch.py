import  os, sys, time, commands
from  signal import SIGTERM

class Daemonize:
    def __init__(self):
        self.pidfile = pidfile

    def daemonize(self):
        try:
            #this process would create a parent and a child
            pid = os.fork()
            if pid > 0:
                #parent proc is okay
                sys.exit(0)
        except OSError, err:
            sys.stderr.write("Fork parent failed --> %d -- [%s]\n" % (err.errno, err.strerror))
            sys.exit(1)

            #change to root
            os.chdir('/')
            #detach from terminal:
            os.setsid()
            #file to be created:
            os.umask(0)

            try:
                #this process creates parent ID
                pid = os.fork()
                if pid > 0:
                    print "Daemon proc id %d" % pid
                    sys.exit(0)
            except OSError, err:
                sys.stderr.write("Fork2 child failed --> %d -- [%s]\n" % (err.errno, err.strerror))
                sys.exit(1)

            sys.stdout.flush()
            sys.stderr.flush()


    def start(self):
        #check if its already running:
        pid_count = commands.getstatusoutput('ps -ef |grep \'aws-watcher.py start\'|grep -v grep|awk \'{print $2}\'|wc -l')
        if int(pid_count[1]) > 1:
            msg="aws-watcher daemon is already running...\n"
            sys.stdout.write(msg)
            sys.exit(1)

        #start daemon:
        msg = "starting aws-watcher daemon...\n"
        sys.stdout.write(msg)
        self.daemonize()
        self.run()

    def stop(self):
        #get pid:
        pid = commands.getstatusoutput('ps -ef |grep \'aws-watcher.py start\'|grep -v grep|awk \'{print $2}\'')
        if pid[1]:
            pid = int(pid[1])
        else:
            pid = None

        if not pid:
            msg = "pid does not exist. Daemon not running?\n"
            #sys.stderr.write(msg)
            sys.exit(1)

        os.system('kill -9 %s' % pid)
        msg = "aws-watcher daemon stopped.\n"
        sys.stdout.write(msg)
        sys.exit(0)


    def run(self):
        #over ride method
        pass
